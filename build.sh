#!/bin/bash

# Build script to automate cross-compilation of custom Qt 5 and PyQt5
# libraries for Debian systems.

set -e

function download_and_unpack {
    DIR=$1
    URL=$2
    ARCHIVE=$3
    if [ ! -e $ARCHIVE ]; then
        wget $URL
    fi

    echo "Unpacking $ARCHIVE"

    if [ ! -e $DIR ]; then
        if [ ${ARCHIVE: -2} == "gz" ]; then
            tar zxf $ARCHIVE
        elif [ ${ARCHIVE: -2} == "xz" ]; then
            tar Jxf $ARCHIVE
        elif [ ${ARCHIVE: -3} == "bz2" ]; then
            tar jxf $ARCHIVE
        else
            echo 'Cannot unpack $ARCHIVE'
            exit 1
        fi
    fi
}

# Define the build configuration.

BUILD_DIR=$PWD
INSTALL_DIR=$PWD/install
# The ROOTFS variable is exported for use in the apply_template.py script.
export ROOTFS=$PWD/rootfs-armhf
DEVICE=linux-rasp-pi2-g++
COMPILER=/usr/bin/arm-linux-gnueabihf-
ARCH=armhf
SUITE=stretch
PYTHON_VERSION=3.5
SIPCONFIG=$PWD/configs/sip4.19-custom.cfg
PYQTCONFIG=$PWD/configs/pyqt5.9-custom.cfg

#SIP_DIR=sip-4.18.1
#QT5_DIR=qt-everywhere-opensource-src-5.6.2
#PYQT5_DIR=PyQt5_gpl-5.6
SIP_DIR=sip-4.19.3
QT5_DIR=qt-everywhere-opensource-src-5.9.1
#QT5_DIR=qtbase
PYQT5_DIR=PyQt5_gpl-5.9
PYQT5_VERSION=5.9

SIP_ARCHIVE=$SIP_DIR.tar.gz
QT5_ARCHIVE=$QT5_DIR.tar.xz
PYQT5_ARCHIVE=$PYQT5_DIR.tar.gz

#SIP_URL=https://sourceforge.net/projects/pyqt/files/sip/sip-4.18.1/$SIP_ARCHIVE
#QT5_URL=https://download.qt.io/official_releases/qt/5.6/5.6.2/single/$QT5_ARCHIVE
#PYQT5_URL=https://sourceforge.net/projects/pyqt/files/PyQt5/PyQt-5.6/$PYQT5_ARCHIVE

SIP_URL=https://sourceforge.net/projects/pyqt/files/sip/sip-4.19.3/$SIP_ARCHIVE
QT5_URL=https://download.qt.io/official_releases/qt/5.9/5.9.1/single/$QT5_ARCHIVE
PYQT5_URL=https://sourceforge.net/projects/pyqt/files/PyQt5/PyQt-5.9/$PYQT5_ARCHIVE

QT5_OPTIONS='-no-opengl -no-xcb -no-eglfs '\
'-no-directfb -linuxfb -no-kms -qpa linuxfb '\
'-skip qt3d '\
'-skip qtactiveqt '\
'-skip qtandroidextras '\
'-skip qtcanvas3d '\
'-skip qtconnectivity '\
'-skip qtdeclarative '\
'-skip qtdoc '\
'-skip qtenginio '\
'-skip qtgraphicaleffects '\
'-skip qtlocation '\
'-skip qtmacextras '\
'-skip qtmultimedia '\
'-skip qtquickcontrols '\
'-skip qtquickcontrols2 '\
'-skip qtscript '\
'-skip qtsensors '\
'-skip qtserialbus '\
'-skip qtserialport '\
'-skip qttools '\
'-skip qttranslations '\
'-skip qtwayland '\
'-skip qtwebchannel '\
'-skip qtwebengine '\
'-skip qtwebsockets '\
'-skip qtwebview '\
'-skip qtwinextras '\
'-skip qtx11extras '\
'-skip qtxmlpatterns '\
'-no-compile-examples '\
'-nomake tests -nomake examples -nomake tools '\
'-prefix /usr -opensource -confirm-license '\
'-sysroot '$ROOTFS' -device '$DEVICE' -device-option'

# Install the tools we will need.

#sudo apt-get install -y ccache curl multistrap python wget python3-dev

# Install the cross-compiler.

#sudo apt-get install 

#sudo dpkg --add-architecture $ARCH
#sudo apt-get update

# Create a rootfs for the target.

if [ ! -e $ROOTFS ]; then
    /usr/sbin/multistrap -a $ARCH -d $ROOTFS -f multistrap/multistrap-$SUITE-$ARCH.conf
fi

if [ -e $SIP_DIR ]; then
    rm -r $SIP_DIR
fi

# Download the packages to install.

download_and_unpack $QT5_DIR $QT5_URL $QT5_ARCHIVE
download_and_unpack $SIP_DIR $SIP_URL $SIP_ARCHIVE
download_and_unpack $PYQT5_DIR $PYQT5_URL $PYQT5_ARCHIVE

echo "Build sip for the host to help build sip and PyQt5 for the target."
echo "We need to use the same version for host and target to ensure compatibility with PyQt."

cd $SIP_DIR
python3 configure.py
make
echo "Copying the sip tool into the main build directory for later use."
cp sipgen/sip ../
cd ..

echo "Unpacking Qt."

if [ ! -e $QT5_DIR ]; then
    tar Jxf $QT5_ARCHIVE
fi

echo "Build Qt for the target."

cd $QT5_DIR
#patch -p0 < ../patches/$QT5_DIR.diff
#patch -p0 < ../patches/$QT5_DIR-$DEVICE.diff
./configure $QT5_OPTIONS CROSS_COMPILE="ccache $COMPILER" -v
make
make install
cd ..

echo "Rebuild sip for the target."

rm -r $SIP_DIR
tar zxf $SIP_ARCHIVE
cd $SIP_DIR
../configs/apply_template.py $SIPCONFIG > sip.cfg
python3 configure.py --sysroot=$ROOTFS --use-qmake --configuration=sip.cfg
../$QT5_DIR/qtbase/bin/qmake
make
make install
cd ..

echo "Build PyQt5 for the target."

cd $PYQT5_DIR
patch -p1 < ../patches/$PYQT5_DIR.diff
../configs/apply_template.py $PYQTCONFIG > pyqt5.cfg
CC='ccache gcc' CXX='ccache g++' python3 configure.py \
    --sysroot=$ROOTFS --configuration=pyqt5.cfg \
    --sip=../sip --sip-incdir=$ROOTFS/usr/include/python$PYTHON_VERSION \
    --qmake=../$QT5_DIR/qtbase/bin/qmake \
    --confirm-license --verbose --no-stubs
make
make install
cd ..
