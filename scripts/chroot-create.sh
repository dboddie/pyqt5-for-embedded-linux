#!/bin/sh

set -e

if [ -z $1 ]; then
  echo "Usage: $0 <chroot directory>"
  exit 1
fi

DEBIAN_MIRROR=http://ftp.no.debian.org/debian
CHROOT=$1

sudo debootstrap jessie $CHROOT $DEBIAN_MIRROR
sudo cp -r . $CHROOT/root/PyQt5-Embedded

#sudo mount -t proc proc $CHROOT/proc
#sudo mount -t sysfs sysfs $CHROOT/sys
#sudo mount -o bind /dev $CHROOT/dev

sudo chroot $CHROOT /root/PyQt5-Embedded/scripts/chroot-root-setup.sh

#sudo umount $CHROOT/proc
#sudo umount $CHROOT/sys
#sudo umount $CHROOT/dev
