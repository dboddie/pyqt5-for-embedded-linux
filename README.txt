PyQt5 for Embedded Linux
------------------------

This is a collection of useful scripts, patches and configuration files for
cross-compiling PyQt5 and its dependencies on Debian systems, starting with
Debian 8 (Jessie). It is not intended to be a comprehensive solution for
producing cross-compiled installations of these components, but merely a
starting point for those who want or need to experiment further.

The build.sh script is used to build Qt 5, sip and PyQt5 for a target
architecture and install the result in a rootfs. The architecture to be
used and the particular configurations of Qt and PyQt are defined in the
script itself, as is the location of the rootfs to be created.

-- 
David Boddie <david@boddie.org.uk>
