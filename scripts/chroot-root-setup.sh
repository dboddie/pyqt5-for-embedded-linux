#!/bin/sh

set -e

apt-get update
apt-get install -y sudo vim less 
useradd -m -s /bin/bash -G sudo build
echo "build   ALL=(ALL:ALL) NOPASSWD: ALL" >> /etc/sudoers
mv /root/PyQt5-Embedded /home/build/
chown -R build.build /home/build/PyQt5-Embedded
su - build
